using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    [SerializeField] private List<Transform> waypoints;
    private int _currentWaypoint = 0;
    [SerializeField] private BoxCollider2D boxCollider;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private LayerMask wallLayer;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private float range;
    [SerializeField] private GameObject playerP;

    public float speed = 0.3f;
    // Start is called before the first frame update
    void Start()
    {
        if (waypoints.Count <= 0) return;
        _currentWaypoint = 0;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Waypoint not reached yet? then move closer
        
            Vector2 p = Vector2.MoveTowards(transform.position,
                                            waypoints[_currentWaypoint].position,
                                            speed);
       
            GetComponent<Rigidbody2D>().MovePosition(p);

        /* if (isFacedToWall())
          {
            Debug.Log("wall");
              GetComponent<Rigidbody2D>().velocity = new Vector2(transform.position.x, -speed * Time.deltaTime);
          }*/

        
        if (enemyInTheWay())
            StartCoroutine(enemyCollisionDelay());


        if (PlayerInSightX() || PlayerInSightY())
        {
            if (isFacedToWall()) return;

            Vector2 p1 = Vector2.MoveTowards(transform.position,
                                          playerP.transform.position,
                                          speed);
            GetComponent<Rigidbody2D>().MovePosition(p1);
            
        }

           if (Vector2.Distance(waypoints[_currentWaypoint].transform.position, transform.position) <= 0.1f)
        {
            _currentWaypoint++;
        }

        // Waypoint reached, select next one
        if (_currentWaypoint != waypoints.Count) return;
        waypoints.Reverse();
        _currentWaypoint = 0;

        // Animation
        Vector2 dir = waypoints[_currentWaypoint].position - transform.position;
        GetComponent<Animator>().SetFloat("DirX", dir.x);
        GetComponent<Animator>().SetFloat("DirY", dir.y);
    }

    private bool PlayerInSightX()
    {
        RaycastHit2D hit =
            Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.x * 0f,
            new Vector3(boxCollider.bounds.size.x * range, boxCollider.bounds.size.y * 0.1f, boxCollider.bounds.size.z),
            0, Vector2.left, 0, playerLayer);
        

        if (hit.collider != null)
          Debug.Log("in sight in x");
     

        return hit.collider != null ;
    }
    private bool PlayerInSightY()
    {
        RaycastHit2D hit =
            Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.y * 0f,
            new Vector3(boxCollider.bounds.size.x * 0.1f, boxCollider.bounds.size.y * range, boxCollider.bounds.size.z),
            0, Vector2.left, 0, playerLayer);
      

        if (hit.collider != null)
            Debug.Log("in sight in y");
  

        return hit.collider != null ;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(boxCollider.bounds.center + transform.right * range * transform.localScale.y * 0f,
          new Vector3(boxCollider.bounds.size.x * 0.1f, boxCollider.bounds.size.y * 3f, boxCollider.bounds.size.z));
    }

    private bool isFacedToWall()
    {
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider.bounds.center + transform.right * 0.1f * transform.localScale.x,
            new Vector3(boxCollider.bounds.size.x * 1.5f, boxCollider.bounds.size.y * 1.7f, boxCollider.bounds.size.z),
            0, Vector2.left, 0, wallLayer);
        return raycastHit.collider != null;
    }
    private bool enemyInTheWay()
    {
        bool way = false;
        RaycastHit2D hitEnemeyY =
          Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.y * 0f,
          new Vector3(boxCollider.bounds.size.x * 0.1f, boxCollider.bounds.size.y * 2f, boxCollider.bounds.size.z),
        0, Vector2.left, 0, enemyLayer);
        RaycastHit2D hitEnemeyX =
            Physics2D.BoxCast(boxCollider.bounds.center + transform.right * range * transform.localScale.x * 0f,
            new Vector3(boxCollider.bounds.size.x * 2f, boxCollider.bounds.size.y * 0.1f, boxCollider.bounds.size.z),
            0, Vector2.left, 0, enemyLayer);

        if (hitEnemeyX.collider != null || hitEnemeyY.collider != null)
        {
            Debug.Log("enemy collided");
            way = true; 
        }
            
        return way;

    }

    IEnumerator enemyCollisionDelay()
    {
        GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(1);
        GetComponent<BoxCollider2D>().enabled = true;
    }


    void OnTriggerEnter2D(Collider2D co)
    {
        if (co.tag == "Player")
        {
            Debug.Log("geoge");
            Destroy(co.gameObject);
        }
    }
}
