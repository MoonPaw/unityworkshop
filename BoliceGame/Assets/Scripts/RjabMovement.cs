using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RjabMovement : MonoBehaviour
{
    private float HorizAxis;
    private float VertAxis;
    private Rigidbody body;
    public float speed;
    public float jumpForce;
    private BoxCollider boxBox;
    public LayerMask groundLayer;
    private Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        boxBox = GetComponent<BoxCollider>();
        animator = GetComponent<Animator>();    
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetBool("IdleToWalk", false);
        HorizAxis = Input.GetAxis("Horizontal");
        VertAxis = Input.GetAxis("Vertical");
        if(HorizAxis != 0 || VertAxis != 0)
        {
          body.velocity = new Vector3(-HorizAxis * speed * Time.deltaTime, body.velocity.y, VertAxis * speed * Time.deltaTime);
          animator.SetBool("IdleToWalk",true);
        }



        if (Input.GetButton("Jump") && isGrounded())
        {
            body.velocity = new Vector3(body.velocity.x, jumpForce, body.velocity.z);
        }
    }

    private bool isGrounded()
    {
        bool res = Physics.BoxCast(boxBox.bounds.center, boxBox.bounds.size, Vector3.down, transform.rotation, 300f, groundLayer);
        return !res;
    }
}
