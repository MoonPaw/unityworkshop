using UnityEngine;
using UnityEngine.SceneManagement;

public class UiManager : MonoBehaviour
{

    [SerializeField] private GameObject pauseUI;
    [SerializeField] private GameObject gameOverUI;


    private void Awake()
    {
        PauseGame(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pauseUI.activeInHierarchy)
                PauseGame(false);
            else
                PauseGame(true);
        }
    }

    //Game over function
    public void GameOver()
    {
        gameOverUI.SetActive(true);
    }

    //Pause menu functions
    public void PauseGame(bool _status)
    {
        pauseUI.SetActive(_status);
        Time.timeScale = System.Convert.ToInt32(!_status);
    }
    public void UnPauseGame()
    {
        PauseGame(false);
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void Quit()
    {
        Application.Quit();
    }
  
    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
