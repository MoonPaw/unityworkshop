using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class PacmanController : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody body;
    private float horzInput;
    private float vertInput;
    public TMP_Text textMeshPro;
    private float score = 0;

    [SerializeField] UiManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(PlayerPrefs.GetString("unsername"));
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        horzInput = Input.GetAxis("Horizontal");
        vertInput = Input.GetAxis("Vertical");

        if (horzInput != 0 || vertInput != 0)
        {
            body.velocity = new Vector3(horzInput * speed, 0, vertInput * speed);
        }
       

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Time.timeScale = System.Convert.ToInt32(false);
            uiManager.GameOver();
            Destroy(body);                             
        }
        if (collision.gameObject.tag == "Coin")
        {          
            Destroy(collision.gameObject);
            AddScore();
        }

    }
    public void AddScore()
    {
        score += 10;
        textMeshPro.SetText(score.ToString());
    }

}
