using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.ReorderableList;
using UnityEngine;
using UnityEngine.AI;

public class EmenyPathing : MonoBehaviour
{
    // Start is called before the first frame update
    public NavMeshAgent agent;
    public float range;
    public Transform player;
    public LayerMask wallL, playerL, groundL;
    public Vector3 wkPoint;
    private bool playerInRange;
    private bool wkSet;
    public float lkpoints;

    

    void Awake()
    {
        player = GameObject.Find("Pacman").transform;
        agent = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update()
    {
        playerInRange = Physics.CheckSphere(transform.position, range, playerL);

        if (!playerInRange) Moving();
        if (playerInRange) Chasing();
    }
    private void Moving()
    {
        if (!wkSet) lookForNodes();
        if(wkSet)
            agent.SetDestination(wkPoint);

        Vector3 dist = transform.position - wkPoint;
        if(dist.magnitude < 2f)
            wkSet = false;
    }
    private void lookForNodes()
    {
        float randomX = Random.Range(-lkpoints, lkpoints);
        float randomZ = Random.Range(-lkpoints, lkpoints);

        wkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (!(Physics.Raycast(wkPoint, transform.right, 4f, wallL) || Physics.Raycast(wkPoint, -transform.right, 4f, wallL) || Physics.Raycast(wkPoint, -transform.forward, 4f, wallL) || Physics.Raycast(wkPoint, transform.forward, 4f, wallL)) && (Physics.Raycast(wkPoint, -transform.up, 2f, groundL)))
        {
            wkSet = true;
           // Debug.Log("way pt found : " + wkPoint);
        }

    }
    private void Chasing()
    {
        agent.SetDestination(player.position);
    }
    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Gizmos.DrawRay(wkPoint, transform.right * 4f);
    }

 
}
