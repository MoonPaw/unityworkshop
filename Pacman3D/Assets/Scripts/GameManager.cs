using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public LayerMask wallL, groundL;
    public Vector3 wkPoint;
    private bool wkSet;
    public float lkpoints;
    private GameObject item;
    
    void Start()
    {
        item = Resources.Load("Coin") as GameObject;
        Instantiate(item, new Vector3(0, 2.5f, 0), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameObject.FindGameObjectWithTag("Coin"))
        {
            if (!wkSet) lookForNodes();
            if (wkSet)
            {
                
                item = Resources.Load("Coin") as GameObject;
                Instantiate(item, wkPoint, Quaternion.identity);
                wkSet = false;
            }
            
                

        }

        
    }

    private void lookForNodes()
    {
        
        float randomX = Random.Range(-lkpoints, lkpoints);
        float randomZ = Random.Range(-lkpoints, lkpoints);

        wkPoint = new Vector3(transform.position.x + randomX, 2.5f, transform.position.z + randomZ);

        if (!(Physics.Raycast(wkPoint, transform.right, 1f, wallL) && Physics.Raycast(wkPoint, -transform.right, 1f, wallL) && Physics.Raycast(wkPoint, -transform.forward, 1f, wallL) && Physics.Raycast(wkPoint, transform.forward, 1f, wallL)) && (Physics.Raycast(wkPoint, -transform.up, 1f, groundL)))
        {
            wkSet = true;
            //Debug.Log("way pt found : " + wkPoint);
        }


    }
    void OnDrawGizmosSelected()
    {
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Gizmos.DrawRay(wkPoint, transform.right * 1f);
    }

   
}
