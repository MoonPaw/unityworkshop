using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [Header ("Health")]
    [SerializeField] private float startingHealth;
    public float currentHealth {get; private set;}
    private Animator anim;
    private bool dead;

    [Header ("iFrames")]
    [SerializeField] private float iFramesDuration;
    [SerializeField] private int flashNumber;

    [Header("Components")]
    [SerializeField] private Behaviour[] components;
    [Header("Audio")]
    [SerializeField] private AudioClip deathSound;
    [SerializeField] private AudioClip hurtSound;
    private SpriteRenderer spriteRend;
    private bool invulnerable;
    private PlayerRespawn playerRespawn;
    // Start is called before the first frame update
    void Awake()
    {
        currentHealth = startingHealth;
        anim = GetComponent<Animator>();
        spriteRend = GetComponent<SpriteRenderer>();
        playerRespawn = GetComponent<PlayerRespawn>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -3f && dead == false)
            isDead();


    }

    public void takeDamage(float _damage)
    {
        if (invulnerable) return;
        currentHealth = Mathf.Clamp(currentHealth - _damage,0 , startingHealth);
     
        if (currentHealth > 0)
        {
            //hurt
            anim.SetTrigger("hurt");
            StartCoroutine(Invunerability());
            SoundManager.instance.PlaySound(hurtSound);
        }
        else
        {
            //dead
            if(!dead)
            {
                    //Deactivate all attached component classes
                    isDead();
            }        
        }
    }
    private void isDead()
    {
        foreach (Behaviour component in components)
            component.enabled = false;
        SoundManager.instance.PlaySound(deathSound);
        anim.SetBool("grounded", true);
        anim.SetTrigger("die");
        dead = true;
        playerRespawn.RespawnCheck();
    }
    public void addHealth(float _value)
    {
       currentHealth = Mathf.Clamp(currentHealth + _value,0 , startingHealth); 
    }
    private IEnumerator Invunerability()
    {
        invulnerable = true;
     Physics2D.IgnoreLayerCollision(10,11,true);
     for (int i = 0; i < flashNumber; i++)
     {
        spriteRend.color = new Color(1, 0 , 0, 0.5f);
        yield return new WaitForSeconds(iFramesDuration / (flashNumber*2));
        spriteRend.color = Color.white;
        yield return new WaitForSeconds(iFramesDuration / (flashNumber*2));
     }
     Physics2D.IgnoreLayerCollision(10,11,false);
      invulnerable = false;
    }
    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
     //Respawn
    public void Respawn()
    {
        dead =false;
        addHealth(startingHealth);
        anim.ResetTrigger("die");
        anim.Play("Ilde");
        StartCoroutine(Invunerability());

        //Activate all attached component classes
        foreach (Behaviour component in components)
            component.enabled = true;
    }
}
