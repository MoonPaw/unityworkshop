using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class Gems : MonoBehaviour
{
    [SerializeField] private int pointValue;
    [SerializeField] private AudioClip gemSound;
    private int collected = 0;
    public TMP_Text textMeshPro;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Gem")
        {
            SoundManager.instance.PlaySound(gemSound);
            collected += pointValue;
            PlayerPrefs.SetInt("score",collected);
            textMeshPro.SetText(collected.ToString());
            collision.gameObject.SetActive(false);
        }
    }
}
