using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTrap : MonoBehaviour
{
    [SerializeField] private float shootCd;
    [SerializeField] private Transform firePoint;
    [SerializeField] private GameObject[] arrows;
    [SerializeField] private AudioClip arrowSound;
    private float cdTimer;
    // Start is called before the first frame update
    private void Shoot()
    {
            cdTimer = 0 ;

            arrows[findArrow()].transform.position = firePoint.position;
            arrows[findArrow()].GetComponent<EnemyProjectile>().ActivateProjectile();
    }

    private int findArrow()
    {
        for(int i=0; i < arrows.Length; i++)
        {
            if(!arrows[i].activeInHierarchy)
            return i;
        }
        return 0;
    }

    // Update is called once per frame
    void Update()
    {
         cdTimer += Time.deltaTime;  
         if(cdTimer >= shootCd)
            Shoot();
        
        
    }
}
