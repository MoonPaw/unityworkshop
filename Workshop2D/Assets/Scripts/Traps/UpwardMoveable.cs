using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpwardMoveable : MonoBehaviour
{
    public float moveRange;
    public float speed;
    private float startPos;
    private bool isReached = false;
    private float endPos;
    private Quaternion rota;
    void Start()
    {
        startPos = transform.position.y;
        rota = transform.rotation;
    }

    void Update()
    {

        float som = startPos + moveRange;
        if ((transform.position.y < som) && !isReached)
            translateForward();
        if (transform.position.y >= som)
        {
            // Debug.Log("reached"); 
            isReached = true;
            endPos = transform.position.y;
        }
        if (isReached && (transform.position.y <= endPos))
        {
            translateBackward();
            if (transform.position.y <= startPos)
                isReached = false;
        }
    }

    public void translateForward()
    {
        //Debug.Log("moving forward"); 
        transform.Translate(0, speed * Time.deltaTime, 0);
    }
    public void translateBackward()
    {
        //Debug.Log("moving backward");              
        transform.Translate(0, -(speed * Time.deltaTime), 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.transform.SetParent(gameObject.transform, true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.transform.parent = null;
        }

    }

}
