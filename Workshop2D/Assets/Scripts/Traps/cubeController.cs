using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cubeController : MonoBehaviour
{
    public float moveRange;
    public float speed;
    private float startPos;
    private bool isReached = false;
    private float endPos;
    private Quaternion rota;
    void Start()
    {
      startPos = transform.position.x; 
      rota = transform.rotation;
    }

    void Update()
    {    

      float som = startPos+moveRange;
         if((transform.position.x < som) && !isReached) 
              translateForward();
         if(transform.position.x >= som)
         {
           // Debug.Log("reached"); 
            isReached = true;
            endPos = transform.position.x;          
         } 
         if(isReached && (transform.position.x <= endPos)) 
         {
           translateBackward();     
           if(transform.position.x <= startPos)
           isReached = false;
         }
    }

    public void translateForward()
    {       
       //Debug.Log("moving forward"); 
        transform.Translate(speed *Time.deltaTime, 0, 0);  
    }
    public void translateBackward()
    { 
       //Debug.Log("moving backward");              
         transform.Translate(-(speed *Time.deltaTime) , 0 , 0);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.transform.SetParent(gameObject.transform, true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.transform.parent = null;
        }

    }


}
